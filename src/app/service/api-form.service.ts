import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiFormService {
  datosApi: any;

  public 

  constructor(
    private http: HttpClient
  ) {
  }

  private httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }
    )
  };

  postFormSave( nombre_comercio: any, nit: any, nombre_contacto: any, telefono_uno: any, 
    telefono_dos: any, correo: any, direccion: any, ciudad: any, departamento: any, 
    observaciones: any, referidor: any, cedula: any, satelite_seccional: any, telefono_referidor: any) {

    this.datosApi = {
      "nombre_comercio": nombre_comercio,
      "nit": nit,
      "nombre_contacto": nombre_contacto,
      "telefono_uno": telefono_uno,
      "telefono_dos": telefono_dos,
      "correo": correo,
      "direccion": direccion,
      "ciudad": ciudad,
      "departamento": departamento,
      "observaciones": observaciones,
      "referidor": referidor,
      "cedula": cedula,
      "satelite_seccional": satelite_seccional,
      "telefono_referidor": telefono_referidor
    };
    
    const datos = JSON.stringify(this.datosApi);

    return this.http.post(environment.API_URL,datos);

  }
}
