import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ApiFormService} from './service/api-form.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title: any = 'Solicitud de Producto';
  nameButton: any = 'Enviar';
  public formGroup: FormGroup;
  submitted = false;

  constructor(
    private  formBuilder: FormBuilder, 
    public http: HttpClient, 
    private  apiFormService: ApiFormService
  ) { }

//Validators.pattern(/^[0-9]+$/), Validators.minLength(8), Validators.maxLength(8)
  ngOnInit() {

    this.formGroup = this.formBuilder.group({
      nombre_comercio: ['', [Validators.required,Validators.pattern(/^[A-Za-zñÑáéíóúÁÉÍÓÚ\s]+$/)]],
      nit: ['', [Validators.required,Validators.pattern(/^[0-9]+$/)]],
      nombre_contacto: ['', [Validators.required]],
      telefono_uno: ['', [Validators.required,Validators.pattern(/^[0-9]+$/),Validators.minLength(10), Validators.maxLength(10)]],
      telefono_dos: ['', [Validators.pattern(/^[0-9]+$/),Validators.minLength(7), Validators.maxLength(7)]],
      correo: ['', [Validators.required,Validators.pattern(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)]],
      direccion: ['', [Validators.required]],
      ciudad: ['', [Validators.required]],
      departamento: ['', [Validators.required,Validators.pattern(/^[A-Za-zñÑáéíóúÁÉÍÓÚ\s]+$/)]],
      observaciones: ['', [Validators.required,Validators.pattern(/^[A-Za-zñÑáéíóúÁÉÍÓÚ\s]+$/)]],
      referidor: ['', [Validators.required,Validators.pattern(/^[A-Za-zñÑáéíóúÁÉÍÓÚ\s]+$/)]],
      cedula: ['', [Validators.required,Validators.pattern(/^[0-9]+$/)]],
      satelite_seccional: ['', [Validators.required,Validators.pattern(/^[A-Za-zñÑáéíóúÁÉÍÓÚ\s]+$/)]],
      telefono_referidor: ['', [Validators.required,Validators.pattern(/^[0-9]+$/),Validators.minLength(7), Validators.maxLength(10)]]
    });
  }

  get f() {
    return this.formGroup.controls;
  }

  onSubmit() {

    this.submitted = true;
    const user = this.formGroup.value;

    const nombre_comercio = user.nombre_comercio;
    const nit = user.nit;
    const nombre_contacto = user.nombre_contacto;
    const telefono_uno = user.telefono_uno;
    const telefono_dos = user.telefono_dos;
    const correo = user.correo;
    const direccion = user.direccion;
    const ciudad = user.ciudad;
    const departamento = user.departamento;
    const observaciones = user.observaciones;
    const referidor = user.referidor;
    const cedula = user.cedula;
    const satelite_seccional = user.satelite_seccional;
    const telefono_referidor = user.telefono_referidor;

    if (this.formGroup.valid) {
      (document.getElementById('btn-save') as HTMLInputElement).disabled = true;
      
      this.nameButton = 'Enviando';
      
      this.apiFormService.postFormSave(nombre_comercio, nit, nombre_contacto, telefono_uno, 
        telefono_dos, correo, direccion, ciudad, departamento, observaciones, referidor, cedula, satelite_seccional, telefono_referidor).subscribe(
        result => {

          console.log(result['codigo']);
          if (result['codigo'] === 0) {
            this.dataLayerSend(window, document, 'script', 'dataLayer', 'GTM-T9H7JGH', 'LeadRedeban');
            Swal.fire({
              type: 'success',
              title: 'Información registrada',
              showConfirmButton: true,
              confirmButtonText: 'Entendido',
              confirmButtonColor: '#dd141d'
            });

            this.formGroup.reset();
            Object.keys(this.formGroup.controls).forEach(key => {
              this.formGroup.controls[key].setErrors(null);
            });
          } 
        },
        error => {
          Swal.fire({
            type: 'error',
            title: 'Error API.'
          });

          this.formGroup.reset();
          Object.keys(this.formGroup.controls).forEach(key => {
            this.formGroup.controls[key].setErrors(null);
          });
        });
      this.nameButton = 'Enviar';

    } else if (this.formGroup.invalid) {
      return;
    }

  }

  dataLayerSend(w, d, s, l, i, e) {
    let f;
    let j;
    let dl;
    w[l] = w[l] || [];

    w[l].push({event: e});
    f = document.getElementsByTagName('script')[0], j = document.createElement('script'), dl = l !== 'dataLayer' ? '&l=' + l : '',
      j.async = true, j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
    f.parentNode.insertBefore(j, f);
  }
}
